package tony.murphy.aertel.activity;

import java.io.InputStream;
import java.net.URL;
import java.util.Stack;

import tony.murphy.R;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

public class AertelActivity 
extends Activity {
	public static final String BASE_URL = "http://www.rte.ie/aertel/images/";
	public static final String EXTENSION_URL = ".gif";
	private static final int DIALOG_TEXT_ENTRY = 0;

	private URL url;
	private ImageView imageView;
	private EditText findPageText;
	private int pageNumber = 100;
	private int subPage = 1;
	private ProgressDialog progressDialog;

	//This is used to save previous Aertel pages viewed
	private Stack<URL> previousPages;

	/** 
	 * Called when the activity is first created 
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Remove the title bar and set the app to fullscreen mode
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, 
				WindowManager.LayoutParams.FLAG_FULLSCREEN );

		setContentView(R.layout.main);
		previousPages = new Stack<URL>();

		// Set the Aertel home page
		imageView = (ImageView) findViewById(R.id.tvtext);
		imageView.setOnClickListener(findPageListener);

		getAertelPage(pageNumber, subPage);

		// Setup the navigation buttons
		final ImageView nextSubpageButton = (ImageView) findViewById(R.id.nextSubpageButton);
		nextSubpageButton.setOnClickListener(nextSubpageListener); 

		final ImageView previousSubpageButton = (ImageView) findViewById(R.id.previousSubpageButton);
		previousSubpageButton.setOnClickListener(previousSubpageListener); 
	}

	/**
	 *  Find Page listener 
	 */
	private OnClickListener findPageListener = new OnClickListener(){ 
		public void onClick(View v) {
			showDialog(DIALOG_TEXT_ENTRY);
		}
	};

	/**
	 *  Next Sub-page button listener 
	 */
	private OnClickListener nextSubpageListener = new OnClickListener(){            
		public void onClick(View v) {
			getAertelPage(pageNumber, subPage+1);
		}
	};

	/**
	 *  Previous Sub-page button listener 
	 */
	private OnClickListener previousSubpageListener = new OnClickListener(){  
		public void onClick(View v) {
			getAertelPage(pageNumber, subPage-1);
		}
	};

	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case DIALOG_TEXT_ENTRY:
			LayoutInflater factory = LayoutInflater.from(this);
			final View textEntryView = factory.inflate(R.layout.find_page_popup, null);
			return new AlertDialog.Builder(AertelActivity.this)
			.setTitle(R.string.findPageString)
			.setView(textEntryView)
			.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int whichButton) {
					try{
						// User clicked OK so do some stuff
						findPageText = (EditText) textEntryView.findViewById(R.id.findPage);
						Log.d("findPageText", findPageText.getText().toString());
						int newPageNumber = Integer.parseInt(findPageText.getText().toString());
						findPageText.setText("");
						getAertelPage(newPageNumber, 1);
					} catch(NumberFormatException e){
						Toast toast = Toast.makeText(getApplicationContext(), "Invalid page number", Toast.LENGTH_LONG);
						toast.show();
					}
				}
			})
			.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int whichButton) {
					// User clicked cancel so do nothing
				}
			}).create();
		}
		return null;
	}

	/**
	 * The back button was pressed. Rather than exit the application, we want to display the 
	 * last viewed page.
	 */
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event)  {
		if (keyCode == KeyEvent.KEYCODE_BACK
				&& event.getRepeatCount() == 0) {
			Log.d("AERTEL", "Back key pressed");
			// If there are no previous pages then let the OS determine what to do
			if(previousPages.empty()){
				Log.d("AERTEL", "No previous pages");
				return super.onKeyDown(keyCode, event);
			}

			Log.d("AERTEL", "Previous page: "+previousPages.peek());

			url = previousPages.pop();
			getAertelPage(url);

			return true;
		}
		if (keyCode == KeyEvent.KEYCODE_BACK
				&& event.getRepeatCount() > 0) {
			return super.onKeyDown(keyCode, event);
		}

		return false;
	}

	private void getAertelPage(int pageNumber, int subPage) {
		try {
			URL prevUrl = url;
			url = new URL(BASE_URL+pageNumber+"-0"+subPage+EXTENSION_URL);
			Log.d("AERTEL", "Getting url: "+url.toString());

			if(getAertelPage(url)){
				this.pageNumber = pageNumber;
				this.subPage = subPage;

				// Push the previous URL to the stack
				if(prevUrl != null){
					Log.d("AERTEL", "Pushing URL :"+prevUrl.toString()+" to stack");
					previousPages.push(prevUrl);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Toast toast = Toast.makeText(getApplicationContext(), "An error occurred, check your internet connection", Toast.LENGTH_LONG);
			toast.show();
		}
	}

	private boolean getAertelPage(final URL url) {
		try {
//			progressDialog = ProgressDialog.show(this, "Loading...", "Loading live Aertel page", true, true);
//
//			Runnable r = new Thread() {
//				public void run() {
//					try{
						Drawable drawable = Drawable.createFromStream((InputStream) url.getContent(), "Aertel Page");
						imageView.setImageDrawable(drawable);
//					} catch (Exception e) { 
//						e.printStackTrace();
//						Toast toast = Toast.makeText(getApplicationContext(), url.toString(), Toast.LENGTH_LONG);
//						toast.show();
//					}
//
//					// When grabbing data is finish: Dismiss your Dialog 
//					progressDialog.dismiss();
//				}
//			};
//			
//			imageView.post(r);

			return true;
		}
		catch (Exception e){
			e.printStackTrace();
			Toast toast = Toast.makeText(getApplicationContext(), "An error occurred, check your internet connection", Toast.LENGTH_LONG);
			toast.show();
		}
		return false;
	}
}
